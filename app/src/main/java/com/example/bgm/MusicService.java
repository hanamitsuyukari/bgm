package com.example.bgm;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class MusicService extends Service {
    private MediaPlayer player;

    @Override
    public void onCreate() {
        super.onCreate();

        //MediaPlayerのオブジェクトを作成
        player = MediaPlayer.create(this,R.raw.yuyakeyori);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        player.stop();
        player.release();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();//再生開始
        player.setLooping(true);//繰り返し再生設定
        return super.onStartCommand(intent, flags, startId);
    }
}
